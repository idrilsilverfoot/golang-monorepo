module github.com/flowerinthenight/golang-monorepo

go 1.14

require (
	github.com/golang/glog v1.0.0
	github.com/pkg/errors v0.9.1
	github.com/spf13/cobra v1.2.1
	github.com/spf13/pflag v1.0.5
)
